package be.kdg.pizzamenu.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import be.kdg.pizzamenu.R
import be.kdg.pizzamenu.model.Pizza

class DetailFragment : Fragment() {

    private lateinit var name: TextView
    private lateinit var diameter: TextView
    private lateinit var price: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        initialiseViews(view)
        return view
    }

    private fun initialiseViews(view: View) {
        name = view.findViewById(R.id.name)
        diameter = view.findViewById(R.id.diameter)
        price = view.findViewById(R.id.price)
    }

    fun setSelectedPizza(pizza: Pizza) {
        name.text = pizza.name
        diameter.text = pizza.diameter.toString()
        price.text = pizza.price.toString()
    }
}
