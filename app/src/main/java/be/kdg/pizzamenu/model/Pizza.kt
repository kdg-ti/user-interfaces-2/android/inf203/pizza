package be.kdg.pizzamenu.model

import java.io.Serializable

data class Pizza(
    val name: String,
    val diameter: Double,
    val price: Double
) : Serializable
