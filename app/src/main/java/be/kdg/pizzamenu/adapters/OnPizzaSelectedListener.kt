package be.kdg.pizzamenu.adapters

interface OnPizzaSelectedListener {
    fun onPizzaSelected(position: Int)
}