package be.kdg.pizzamenu.adapters

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.kdg.pizzamenu.R

class PizzaViewHolder(view: View)
    : RecyclerView.ViewHolder(view) {

    val pizzaName = view.findViewById<TextView>(R.id.pizzaName)
    val diameter = view.findViewById<TextView>(R.id.size)
    val price = view.findViewById<TextView>(R.id.price)
}